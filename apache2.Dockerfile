# pull latest centos7 image
FROM centos:7

MAINTAINER Nicholas M. Cifranic

# variable named ${PORT} - default port on which the Apache server will be listening
ENV PORT 8080

RUN yum update -y && \
    yum install -y httpd

RUN sed -ri -e '/^Listen 80/c\Listen ${PORT}' /etc/httpd/conf/httpd.conf && \
    chown -R apache:apache /etc/httpd/logs/ && \
    chown -R apache:apache /run/httpd/

# switch (su) to user named 'apache'
USER apache

# expose the port of which was declared ealier
EXPOSE ${PORT}

# creates a mount point for the HTML files that the Apache server will host. When the container is run, this instruction notifies a user reading the Dockerfile where a -v option can be used to mount files from the host to the container. 
VOLUME /var/www/html

# starts the Apache server in the forground when the container is run: 
CMD ["httpd", "-D", "FOREGROUND"]





