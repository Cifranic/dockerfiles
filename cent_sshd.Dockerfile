FROM       centos:7
MAINTAINER Nicholas M. Cifranic "https://github.com/cifranic"

RUN yum -y update

RUN yum -y install openssh-server
RUN mkdir /var/run/sshd

RUN echo 'root:root' |chpasswd

RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config

RUN mkdir /root/.ssh

EXPOSE 22

CMD    ["/usr/sbin/sshd", "-D"]
