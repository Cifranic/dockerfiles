# IMPORTANT: Rename these files to "Dockerfile" before building image
# To build:
#            1.  Navigate to directory of the Dockerfile
#            2. $ docker build -t <tastefull_docker_image_name> .

FROM centos

MAINTAINER Nick Cifranic nicifranic@gmail.com

LABEL description="A basic Apache container on Centos7"

RUN yum -y update && \
    yum install -y httpd && \
    yum clean all

EXPOSE 80

#set httpd as the default executable when the container is run: 
CMD ["httpd", "-D", "FOREGROUND"]


